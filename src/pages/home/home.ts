import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { MessageProvider } from '../../providers/message/message';
declare var google;
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/observable/interval';
import { isRightSide } from 'ionic-angular/umd/util/util';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  Userlocation: any = '';
  Driverlocation: any;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
 currentPositionMarker
//map

  constructor(public navCtrl: NavController,
    public toastmessage:MessageProvider,
    public geolocation:Geolocation) {
   
  }

  ngAfterViewInit(){
   this.initLocationProcedure();
//this.loadMap();
  }

              initializeMap()
              {
              //  let latLng1=new google.maps.LatLng(9.7679223,76.4885526);
               let latLng1=new google.maps.LatLng(12.8450867,77.658752);
                     this.Userlocation=latLng1
                  this.map = new google.maps.Map(document.getElementById('map_canvas'), {
                     zoom: 13,
                     center: this.Userlocation,
                     mapTypeId: google.maps.MapTypeId.ROADMAP
                   });
                   let usermarker = new google.maps.Marker({
                    map: this.map,
                    position:this.Userlocation, 
                    icon:"assets/imgs/house.png"
                });
              }
  
             locError(error) {
                  // the current position could not be located
                  alert("The current position could not be found!");
              }
  
            setCurrentPosition(pos) {
                  this.currentPositionMarker = new google.maps.Marker({
                      map: this.map,
                      position: new google.maps.LatLng(
                          pos.coords.latitude,
                          pos.coords.longitude
                      ),
                      icon:"assets/imgs/delivery-bike.png"
                  });
                  this.map.panTo(new google.maps.LatLng(
                          pos.coords.latitude,
                          pos.coords.longitude
                      ));
              }
  
           displayAndWatch(position) {
             console.log("posss"+JSON.stringify(position))
                  // set current position
                  this.setCurrentPosition(position);
                  // watch position
                  this.watchCurrentPosition();
              }
  
               watchCurrentPosition() {
                  var positionTimer =this.geolocation.watchPosition();
                  positionTimer.subscribe((position) => {
                       
                          this.setMarkerPosition(
                              this.currentPositionMarker,
                              position
                          )
                      var Driverlocation=   new google.maps.LatLng(position.coords.latitude,
                        position.coords.longitude);
                        
                          this.calculateAndDisplayRoute(Driverlocation)
                      })
                    
              }
  
              setMarkerPosition(marker, position) {
                  marker.setPosition(
                      new google.maps.LatLng(
                          position.coords.latitude,
                          position.coords.longitude)
                  );
              }
  
             initLocationProcedure() {
                  this.initializeMap();
                  // if (navigator.geolocation) {
                    this.geolocation.getCurrentPosition().then((position) => {
                      
           this.displayAndWatch(position)
                    })
                  // } else {
                  //     alert("Your browser does not support the Geolocation API");
                  // }
              }
//marker click
  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

  calculateAndDisplayRoute(Driverlocation) {
 //   alert("haiiiiiiii");
   
   
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true,
    
    });
    // directionsDisplay.setMap(null);
    // const map = this.map
    // directionsDisplay.setMap(map);
    const map = this.map
if(directionsDisplay != null) {
  directionsDisplay.setMap(null);
  directionsDisplay = null;
}
else
{
  directionsDisplay.setMap(map);
}



    directionsService.route({
    origin: Driverlocation,
    destination: this.Userlocation,
    travelMode: 'DRIVING',
    
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

}
    

