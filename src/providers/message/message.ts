
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class MessageProvider {
  public loading : any
  public toasts : any;
  postUrl;
  notificount;
  userToken1;
  constructor(public toast : ToastController, 
    public loadingCtrl : LoadingController,) {
      this.userToken1 = window.localStorage.getItem('idTokenUser');
    console.log('Hello MessageProvider Provider');
  }

public toatMessages(revmsg):any{
  let toast = this.toast.create({
            message: revmsg,
            duration: 2000,
            position:'bootom',
          //  cssClass : "toastBg toast-success ",
        });
        toast.present();
        return true;
}

  public loadingFunction(rec_message):any {
    this.loading = this.loadingCtrl.create({
      content: rec_message,
      spinner: 'bubbles'                                                                                                                                                                                                                    
  });
  this.loading.present();
  }
 


  public loadingDismiss() : void{
    this.loading.dismiss();
  }
}
